﻿using alquiler_autos.Entidades;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alquiler_autos.Context
{
    public class AppBDContext:DbContext
    {
        public AppBDContext(DbContextOptions<AppBDContext> options) :base(options)
        {          
        }

        public DbSet<Autos> Autos { get; set; }
        public DbSet<Ordenes> Ordenes { get; set; }
    }
}
