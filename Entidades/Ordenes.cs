﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace alquiler_autos.Entidades
{
    public class Ordenes
    {
        [Key]
        public string id_orden { get; set; }
        public string nombre_cliente { get; set; }
        public string telefono_cliente { get; set; }
        public string cedula_cliente { get; set; }
        public string id_auto { get; set; }
        public int dias_alquiler { get; set; }

    }
}
