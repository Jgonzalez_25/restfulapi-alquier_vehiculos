﻿using alquiler_autos.Context;
using alquiler_autos.Entidades;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace alquiler_autos.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AutosController : ControllerBase 
    {
        private readonly AppBDContext context;
        public AutosController(AppBDContext context)
        {
            this.context = context;
        }
        // GET: api/<AutosController>
        [HttpGet]
        public IEnumerable<Autos> Get()
        {
            return context.Autos.ToList();
        }

        // GET api/<AutosController>/5
        [HttpGet("{id}")]
        public Autos Get(string id)
        {
            var Autos = context.Autos.FirstOrDefault(a => a.id_auto == id);
                return Autos;
        }

        // POST api/<AutosController>
        [HttpPost]
        public ActionResult Post([FromBody] Autos autos)
        {
            try
            {
                context.Autos.Add(autos);
                context.SaveChanges();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
            
        }

        // PUT api/<AutosController>/5
        [HttpPut("{id}")]
        public ActionResult Put(string id, [FromBody] Autos autos)
        {
            if (autos.id_auto == id)
            {
                context.Entry(autos).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                context.SaveChanges();
                return Ok();
            }
            else
                return BadRequest();
        }

        // DELETE api/<AutosController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            var auto = context.Autos.FirstOrDefault(a => a.id_auto == id);
            if (auto != null)
            {
                context.Autos.Remove(auto);
                context.SaveChanges();
                return Ok();
            }
            else
                return BadRequest();
        }
    }
}
