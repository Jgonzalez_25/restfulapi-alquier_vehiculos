﻿using alquiler_autos.Context;
using alquiler_autos.Entidades;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace alquiler_autos.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdenesController : ControllerBase
    {
        private readonly AppBDContext context;
        public OrdenesController(AppBDContext context)
        {
            this.context = context;
        }
        // GET: api/<OrdenesController>
        [HttpGet]
        public IEnumerable<Ordenes> Get()
        {
            return context.Ordenes.ToList();
        }

        // GET api/<OrdenesController>/5
        [HttpGet("{id}")]
        public Ordenes Get(String id)
        {
            var orden = context.Ordenes.FirstOrDefault(o => o.id_orden == id);
            return orden;
        }

        // POST api/<OrdenesController>
        [HttpPost]
        public ActionResult Post([FromBody] Ordenes orden)
        {
            try
            {
                context.Ordenes.Add(orden);
                context.SaveChanges();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        // PUT api/<OrdenesController>/5
        [HttpPut("{id}")]
        public ActionResult Put(string id, [FromBody] Ordenes orden)
        {
            if (orden.id_orden == id)
            {
                context.Entry(orden).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                context.SaveChanges();
                return Ok();
            }
            else
                return BadRequest();
        }

        // DELETE api/<OrdenesController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            var orden = context.Ordenes.FirstOrDefault(o => o.id_orden == id);
            if (orden != null)
            {
                context.Ordenes.Remove(orden);
                context.SaveChanges();
                return Ok();
            }
            else
                return BadRequest();
        }
    }
}
