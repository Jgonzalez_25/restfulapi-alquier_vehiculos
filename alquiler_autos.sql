use alquiler_autos

create table autos (
id_auto varchar(2) not null primary key,
marca varchar(25),
modelo varchar (25),
precio decimal(8,2))

create table ordenes (
id_orden varchar(2) not null primary key,
nombre_cliente varchar(50),
telefono_cliente varchar(12),
cedula_cliente varchar(12),
id_auto varchar(2),
dias_alquiler int,
constraint FK_orden_auto foreign key (id_auto) references autos (id_auto))